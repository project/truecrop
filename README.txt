TRUE CROP
---------

This module allows you to generate new images by cropping existing ones. Because the cropped images are first-class file entities, they will appear (and can be reused) anywhere else a regular image file would be.

For additional information, see the project page: https://www.drupal.org/project/truecrop
